<?php

namespace Drupal\Tests\webform_pardot\Functional;

use Drupal\webform\Entity\Webform;
use Drupal\Tests\webform\Functional\WebformBrowserTestBase;
use Drupal\Core\Config\FileStorage;

/**
 * Tests configuring the form handler.
 *
 * @group webform_pardot
 */
class ConfigurationTest extends WebformBrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'webform',
    'webform_pardot',
  ];

  /**
   * Webforms to load.
   *
   * @var array
   */
  protected static $testWebforms = ['multi_step_pardot_test_form'];

  /**
   * Lazy load a test webform.
   *
   * @param string $id
   *   Webform id.
   *
   * @return \Drupal\webform\WebformInterface|null
   *   A webform.
   *
   * @see \Drupal\views\Tests\ViewTestData::createTestViews
   */
  protected function loadWebform($id) {
    $storage = \Drupal::entityTypeManager()->getStorage('webform');
    if ($webform = $storage->load($id)) {
      return $webform;
    }
    else {
      $config_name = 'webform.webform.' . $id;
      $config_directory = \Drupal::service('extension.list.module')->getPath('webform_pardot') . '/tests/modules/test/config/install';
      if (!file_exists("$config_directory/$config_name.yml")) {
        throw new \Exception("Webform $id does not exist in $config_directory");
      }

      $file_storage = new FileStorage($config_directory);
      $values = $file_storage->read($config_name);
      $webform = $storage->create($values);
      $webform->save();
      return $webform;
    }
  }

  /**
   * Tests a multi-step form.
   */
  public function testEditHandler() {
    $this->drupalLogin($this->rootUser);

    $webformId = 'multi_step_pardot_test_form';
    $handlerId = 'submit_data_to_pardot';
    $handlerEditPath = "/admin/structure/webform/manage/{$webformId}/handlers/{$handlerId}/edit";
    $this->drupalGet($handlerEditPath);

    // Invalid URLs should not be allowed.
    $this->submitForm([
      'settings[pardot_url]' => 'foo',
    ], 'Save');
    $this->assertSession()->pageTextContains('Pardot post url must be a valid URL.');
    $this->drupalGet($handlerEditPath);

    // Valid URLs with extra whitespace should be accepted, but the whitespace
    // should be trimmed.
    $this->submitForm([
      'settings[pardot_url]' => ' https://example.com/ ',
    ], 'Save');
    $this->assertSession()->pageTextContains('The webform handler was successfully updated.');

    $storage = \Drupal::entityTypeManager()->getStorage('webform');
    $storage->resetCache([$webformId]);
    /** @var \Drupal\webform\Entity\Webform $webform */
    $webform = $storage->load($webformId);
    $handler = $webform->getHandler($handlerId);
    $pardot_url = $handler->getSetting('pardot_url');
    $this->assertTrue($pardot_url === 'https://example.com/', 'Whitespace is trimmed from the Pardot URL.');
    $this->drupalGet($handlerEditPath);

    $this->submitForm([
      'settings[pardot_url]' => 'https://example.com/pardot',
    ], 'Save');
    $this->assertSession()->pageTextContains('The webform handler was successfully updated.');
  }

}
