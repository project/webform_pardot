# Webform Pardot Handler

The webform_pardot_hander module posts webform submission to
[Pardot](https://www.pardot.com/). While Webform's remote_post 
handler can also be used with Pardot, this module offers a number 
of advantage including;
* Webform submissions are queued and subitted when cron is run 
  improving site performance
* A pardot submission entity for each pardot submission viewable 
  at `/admin/structure/webform/pardot_submissions`.
* Errors returned by the Pardot Endpoint URL are logged

## Requirements

* [Webform](https://www.drupal.org/project/webform)
* Cron 

## Installation

* Install using composer. `composer require drupal/webform_pardot_handler`

## Configuration
 
* Under the Email/Handlers section of the webform 
  (admin/structure/webform/manage/{webform_id}/handlers)
  Click Add handler
* Select `Pardot handler`
* Fill in the Pardot Form Handler Enpoint URL
* Add any field mapptings using the webform_key|pardot_key pattern

## Mantainers

Current maintainer:
* Kevin Reynen - https://www.drupal.org/u/kreynen

Previous maintainers:
* Safallia Joseph - https://www.drupal.org/u/safallia-joseph
* Travis Bradbury (bradbury) - https://www.drupal.org/u/tbradbury
